/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import javax.swing.JOptionPane;
import util.ufps.colecciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class PruebaReemplazar {
    
    public static void main(String[] args) {
        ListaCD<Character> l1 = leerCadena("Leer cadena original L1");
        ListaCD<Character> l2 = leerCadena("Leer cadena original L2-patron");
        ListaCD<Character> l3 = leerCadena("Leer cadena original L3-nueva");
        /**
         * Crear las 3 listas
         */

        System.out.println("Lista Original:" + l1.toString());
        int veces = l1.reemplazar(l2, l3);
        System.out.println("Se realizaron:" + veces + " reemplazos y la lista quedó:");
        System.out.println(l1.toString());

    }

    private static ListaCD<Character> leerCadena(String mensaje) {
        ListaCD<Character> l = new ListaCD();
        String cadena = JOptionPane.showInputDialog(mensaje);
        for (int i = 0; i < cadena.length(); i++) {
            l.insertarFin(cadena.charAt(i));
        }
        return l;
    }

}
