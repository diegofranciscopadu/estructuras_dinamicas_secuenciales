/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import util.ufps.colecciones_seed.ListaS;

/**
 *
 * @author madarme
 */
public class PruebaListaSimple {
    
    
    public static void main(String[] args) {
        ListaS<String> nombres=new ListaS();
        nombres.insertarInicio("Cesar");
        nombres.insertarInicio("María");
        nombres.insertarInicio("Andres");
        nombres.insertarInicio("Fabián");
        System.out.println("\nLista de String:");
        System.out.println("Mi lista es:"+nombres.toString());
        System.out.println("Mi lista tiene:"+nombres.getTamanio()+" elementos");
        
        ListaS<Integer> numeros=new ListaS();
        numeros.insertarFin(10);
        numeros.insertarFin(100);
        numeros.insertarFin(1000);
        numeros.insertarFin(10000);
        numeros.insertarFin(10000);
        System.out.println("\nLista de números:");
        System.out.println("Mi lista es:"+numeros.toString());
        System.out.println("Mi lista tiene:"+numeros.getTamanio()+" elementos");
        System.out.println("Cambiando el dato pos=2 por su valor*2");
        numeros.set(2, numeros.get(2)*2);
        System.out.println("Su nuevo valor del dato en pos2 es:"+numeros.get(2));
        System.out.println("Mi lista es:"+numeros.toString());
        
        System.out.println("\nProbando método Eliminar: en pos=2");
        System.out.println("Dato borrado:"+numeros.eliminar(2));
        System.out.println("Mi lista es:"+numeros.toString());
        System.out.println("Mi lista tiene:"+numeros.getTamanio()+" elementos");
        
        
        
    }
}
