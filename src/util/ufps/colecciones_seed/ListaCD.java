/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.ufps.colecciones_seed;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que representa una lista circular doblemente enlazada con nodo
 * centinela o nodo cabecera
 *
 * @author madarme
 */
public class ListaCD<T> {

    private NodoD<T> cabeza;
    private int tamanio = 0;

    public ListaCD() {
    
        this.crearNodoCentinela();
    }

    private void crearNodoCentinela()
    {
        //Crear nodoCabeza:
        this.cabeza = new NodoD();
        //crear sus enlaces dobles circulares:
        this.cabeza.setSig(cabeza);
        this.cabeza.setAnt(cabeza);
    }
    
    public void borrarLista() {
        this.tamanio = 0;
        this.crearNodoCentinela();

    }

    public int getTamanio() {
        return tamanio;
    }

    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabeza.getSig(), this.cabeza);
        nuevo.getSig().setAnt(nuevo);
        this.cabeza.setSig(nuevo);
        this.tamanio++;
    }

    public void insertarFin(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabeza, this.cabeza.getAnt());
        this.cabeza.setAnt(nuevo);
        nuevo.getAnt().setSig(nuevo);
        this.tamanio++;
    }

    @Override
    public String toString() {
        String msg = "";

        for (NodoD<T> nodoActual = this.cabeza.getSig(); nodoActual != this.cabeza; nodoActual = nodoActual.getSig()) {
            msg += nodoActual.getInfo().toString() + "<->";
        }
        return "Cabeza->" + msg + "Cabeza";

    }

    public T eliminar(int i) {
        try {
            NodoD<T> nodoBorrar = this.getPos(i);
            nodoBorrar.getAnt().setSig(nodoBorrar.getSig());
            nodoBorrar.getSig().setAnt(nodoBorrar.getAnt());
            nodoBorrar.setSig(null);
            nodoBorrar.setAnt(null);
            this.tamanio--;
            return nodoBorrar.getInfo();

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    private NodoD<T> getPos(int i) throws Exception {
        if (i < 0 || i >= this.tamanio) {
            throw new Exception("Índice fuera de rango");
        }

        NodoD<T> nodoPos = this.cabeza.getSig();

        while (i-- > 0) {
            nodoPos = nodoPos.getSig();

        }
        return nodoPos;
    }

    public T get(int i) {

        try {
            return this.getPos(i).getInfo();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }

    public void set(int i, T datoNuevo) {

        try {
            this.getPos(i).setInfo(datoNuevo);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());

        }
    }

    public boolean esVacia() {
        //return this.tamanio==0
        return this.cabeza == this.cabeza.getSig() && this.cabeza == this.cabeza.getAnt();
    }
    
    
    /**
     * Ejercicio #14 de la hoja de listas
     * 
     * Restricciones:
     *  1. NO SE PUEDE UTILIZAR VECTORES 
     *  2. NO SE PUEDE UTILIZAR LOS MÉTODOS GET, SET , GETPOS, ELIMINAR, INSERTAR INICIO O INSERTAR FIN
     * 
     * TIP: SE DEBE CREAR TANTAS LISTAS L3(NUEVAS) COMO SEAN NECESARIAS PARA LOS REEMPLAZOS
     * 
     * @param l2 Lista que se debe buscar en la lista original (l2)
     * @param l3 Lista que se debe reemplazar por el patrón (l3)
     * @return  la cantidad de reemplazos que se realizaron
     */
    
    public int reemplazar (ListaCD<T> l2, ListaCD<T> l3)
    {
      
      int contador=0;
       
       NodoD<T> iteradorL1= this.cabeza.getSig();//en el primer elemento
       while(!iteradorL1.equals(this.cabeza))//se recorre mientras que no llegue al final que es donde termina
       {
             boolean semaforo=true;
             NodoD<T> nodoActual= iteradorL1.getAnt();//para saber el anterior de donde va el iterador para saber donde remplazar desde l3 a l1
             NodoD<T> iteradorL2 = l2.cabeza.getSig();//igual que el primer iterador
             
             while(semaforo && !iteradorL2.equals(l2.cabeza)){
             
             if(!iteradorL1.getInfo().equals(iteradorL2.getInfo())){
                    semaforo = false;
                    
                }else{
                    iteradorL2 = iteradorL2.getSig();
                    iteradorL1 = iteradorL1.getSig();
                }
            }
            if(!semaforo) iteradorL1 = nodoActual.getSig().getSig();
            else{//encontró patron
                contador++;//se aumenta el contador porque se encontró una ocurrencia
                ListaCD<T> copia = l3.copiar();
                iteradorL1.setAnt(copia.cabeza.getAnt());
                copia.cabeza.getAnt().setSig(iteradorL1);                
                nodoActual.setSig(copia.cabeza.getSig());
                copia.cabeza.getSig().setAnt(nodoActual);
                copia.cabeza.setAnt(null);
                copia.cabeza.setSig(null);
            }   
        }        
        return contador;
        
    }
    
    
    public ListaCD<T> copiar()
    { 
    
       ListaCD<T> nueva = new ListaCD<>();
        NodoD<T> aux = nueva.cabeza;
        NodoD<T> iterador = this.cabeza.getSig();
        
        while(!iterador.equals(this.cabeza)){
            NodoD<T> nuevo = new NodoD<>();
            nuevo.setInfo(iterador.getInfo());
            nuevo.setAnt(aux);
            aux.setSig(nuevo);
            iterador = iterador.getSig();
            aux = aux.getSig();
        }
        aux.setSig(nueva.cabeza);
        nueva.cabeza.setAnt(aux);
        return nueva;
    }
}
