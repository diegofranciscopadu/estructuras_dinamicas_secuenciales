/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.ufps.colecciones_seed;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class ListaS<T> {

    //Inicialización OPCIONAL, SE REALIZA POR CONCEPTO
    private Nodo<T> cabeza = null;
    private int tamanio = 0;

    public ListaS() {
    }

    public int getTamanio() {
        return tamanio;
    }

    /**
     * Método para insertar un elemento nuevo en el inicio de la lista simple
     *
     * @param datoNuevo un objeto para la listaS
     */
    public void insertarInicio(T datoNuevo) {
        /**
         * Estrategia computacional(Algoritmo): 1. Crear nodoNuevo=new
         * (dato,cabeza); 2. aumentar tamanio 3. Cambiar cabeza a nuevoNodo
         */

        Nodo<T> nodoNuevo = new Nodo(datoNuevo, this.cabeza);//SE CREA EL NUEVO NODO 
        this.cabeza = nodoNuevo;//ENTONCES LO QUE HAGO ES ASIGNAR LA REFERENCIA DEL NODO CABEZA AL  NUEVO NODO.
        this.tamanio++;//Y PUES POR LOGICA TENGO QUE AUMENTAR EL TAMAÑO DE LA LISTA, SI NO, ME SALE INDICE FUERA DE RANGO Y QUE MI LISTA 
        //TA VACIA

    }

    public void insertarFin(T datoNuevo) {
        if (this.esVacia()) {
            this.insertarInicio(datoNuevo);
        } else {
            Nodo<T> nodoNuevo = new Nodo(datoNuevo, null);//LO MISMO QUE PASA ARRIBA

            try {
                Nodo<T> nodoAnt_Actual = getPos(this.tamanio - 1);
                nodoAnt_Actual.setSig(nodoNuevo);
                this.tamanio++;
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
        }
    }

    public T get(int i) {

        try {
            return this.getPos(i).getInfo();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }

    public void set(int i, T datoNuevo) {

        try {
            this.getPos(i).setInfo(datoNuevo);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());

        }
    }

    /**
     * Método elimina el nodo que se encuentra en una posición dada
     *
     * @param i posición del nodo en la lista simple
     * @return el objeto almacenado en el nodo con posición i
     */
    public T eliminar(int i) {
        if (this.esVacia()) {
            return null;
        }
        Nodo<T> nodoBorrar = null;
        if (i == 0) {
            nodoBorrar = this.cabeza;
            this.cabeza = this.cabeza.getSig();
        } else {
            try {
                Nodo<T> nodoAnt = this.getPos(i - 1);
                nodoBorrar = nodoAnt.getSig();
                nodoAnt.setSig(nodoBorrar.getSig());

            } catch (Exception ex) {
                System.err.println(ex.getMessage());
                return null;
            }
        }
        nodoBorrar.setSig(null);
        this.tamanio--;
        return nodoBorrar.getInfo();
    }

    private Nodo<T> getPos(int i) throws Exception {
        if (i < 0 || i >= this.tamanio) {
            throw new Exception("Índice fuera de rango");
        }

        Nodo<T> nodoPos = this.cabeza;

        while (i-- > 0) {
            nodoPos = nodoPos.getSig();
            
        }
        return nodoPos;
    }

    @Override
    public String toString() {
        String msg = "";

        for (Nodo<T> nodoActual = this.cabeza; nodoActual != null; nodoActual = nodoActual.getSig()) {
            msg += nodoActual.getInfo().toString() + "->";
        }
        return "Cabeza->" + msg + "null";

    }

    public boolean esVacia() {
        return this.cabeza == null;
    }

    /**
     * Elimina de la lista todo dato que esté repetido (NO SE DEJA NINGUNO) NO
     * SE PUEDE LLAMAR N VECES AL MÉTODO REPETIR NO SE PUEDE USAR EL MÉTODO GET
     * O SET NO SE PUEDE USAR EL MÉTODO GETPOS
     *
     * Ejemplo: L=<2,3,4,2,1,9,2,1,3>
     * L.eliminarRepetido()--> L=<4,9>
     */
    public void eliminarRepetido() {
        // :)
    }

    /**
     * Método que permite mover el mayor elemento de la lista al final. El
     * proceso sólo será válido si existe un ÚNICO MAYOR Restricciones: 1. Lista
     * no vacía 2. No se pueden crear nodos ( new Nodo...) 3. SÓLO SE PUEDE
     * REALIZAR EL PROCESO EN UN CICLO 4. No se puede utilizar los métodos:
     * getpos, eliminar o adicionar
     *
     * @return
     */
    public boolean moverMayor_al_fin() {
        /**
         *
         * Casos: 1. L=<> --> L.moverMayor_al_Fin()--> "false" 
         * 2. L=<2,4,56,7>
         * --> L.moverMayor() --> L=<2,4,7,56> , "true"-->mitad 
         * 3.
         * L=<2,4,56,7,56> --> L.moverMayor() --> L=<2,4,56,7,56>,"false" 
         * 4.
         * L=<2> -->L.moverMayor()-->L=<2> , "false"
         * 5. L=<112,4,56,7>-->
         * L.moverMayor()-->L=<4,56,7,112>, "true"--> "cabeza".
         * 6.L=<112,4,56,711>--> L.moverMayor()-->L=<112,4,56,711>, "true"-->
         * "final".
         */

        //Caso 1 y 4
        if (this.esVacia() || this.getTamanio() == 1) {
            throw new RuntimeException("No tiene el tamaño adecuado para realizar el proceso");
        }

        //Referenciando y/o inicializando
        //L={2000,30,40,30,50,4000,1,2000,1,2,50,100,20};--> :(
        Nodo<T> nodoMayor = this.cabeza;
        Nodo<T> nodoAnt_Actual = null;
        boolean repetido = false;
        Nodo<T> nodoAnt_mayor = null;
        for (Nodo<T> nodoActual = this.cabeza.getSig(); nodoActual != null; nodoActual = nodoActual.getSig()) {
            /**
             * esto es un grave grave error: if(nodoactual > nodomayor)
             * nodomayor=nodoactual
             */
            int comparador = ((Comparable) nodoActual.getInfo()).compareTo(nodoMayor.getInfo());
            if (comparador > 0) {
                nodoAnt_mayor = nodoAnt_Actual;
                nodoMayor = nodoActual;
                repetido = false;
            } else {
                if (comparador == 0) {
                    repetido = true;
                }
            }
            nodoAnt_Actual = nodoActual;
        } //nodoActual=null, nodoAnt_actual está en el último
        //Caso 3.
        if (repetido) {
            throw new RuntimeException("Elemento mayor repetido, no se puede realizar el proceso");
        }
        //Caso 5:
        if (nodoMayor.getSig() != null) {
            if (nodoMayor == this.cabeza) {
                this.cabeza = nodoMayor.getSig();

            } else {
                nodoAnt_mayor.setSig(nodoMayor.getSig());

            }

            nodoAnt_Actual.setSig(nodoMayor);
            nodoMayor.setSig(null);
        }
        return !repetido;
    }

}
